Essayez de définir vos Paramètres de Packs de Ressources dans la partie Nouveau serveur du menu multijoueur sur ‘Activés’ puis relancez le jeu.

Si cela ne marche pas, alors allez dans votre dossier `%appdata%/roaming/.minecraft` (Cela peut être différent dépendant du joueur, si ce n'est pas ici, nous ne pouvons pas vous aider car c'est le dossier par défaut). Trouvez server-resourcepacks et videz-le. Redémarrez le jeu et réessayez.

Si vous avez tout essayé et que cela ne fonctionne toujours pas, ouvrez un ticket en utilisant l'intégration ci-dessous.

