Tout d'abord vérifiez si le serveur est bien en ligne en faisant `/status` dans <#355479832722669571> , si c'est le cas alors vérifiez que vous utilisez la bonne adresse IP (Utilisez  `/ip` dans <#355479832722669571>. le :25565 n'est pas nécessaire.) et que les packs de ressources sont définis sur Demander.

Si vous avez encore des problèmes alors dîtes dans general que vous avez des problèmes de connexion.

