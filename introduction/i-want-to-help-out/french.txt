Nous acceptons toujours de nouveaux membres dans l'équipe ! Nous vous recommandons d'abord de vérifier [ce beau guide](https://bit.ly/mrclsag) compilé pour vous aider à mieux comprendre notre gestion du personnel et nos normes d'application.

Vous pouvez utiliser le [Formulaire de Candidature](https://bit.ly/mrclsa) pour postuler (Les deux liens sont trouvables également dans <#356883834543013889>)
